USE [PertaminaCheckList]
GO
/****** Object:  User [user1]    Script Date: 11/22/2017 9:43:29 AM ******/
CREATE USER [user1] FOR LOGIN [user1] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [user1]
GO
/****** Object:  Table [dbo].[cl_checklist]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_checklist](
	[CL_ID] [int] IDENTITY(1,1) NOT NULL,
	[CR_ID] [int] NOT NULL,
	[CL_KRITERIA_PERIKSA] [varchar](20) NOT NULL,
	[CL_TGL_MULAI] [date] NULL,
	[CL_TGL_STATUS] [date] NULL,
	[CL_STATUS] [varchar](20) NULL,
	[U_ID] [varchar](30) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_checklist_attachment]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_checklist_attachment](
	[ATT_ID] [int] IDENTITY(1,1) NOT NULL,
	[CL_ID] [int] NOT NULL,
	[CI_ID_1] [int] NOT NULL,
	[ATT_PATH] [varchar](250) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_checklist_hasil]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_checklist_hasil](
	[HSL_ID] [int] IDENTITY(1,1) NOT NULL,
	[CL_ID] [int] NOT NULL,
	[HSL_NOTE_TYPE] [varchar](20) NULL,
	[HSL_NOTE] [varchar](400) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_checklist_request]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_checklist_request](
	[CR_ID] [int] IDENTITY(1,1) NOT NULL,
	[MBL_ID] [varchar](12) NOT NULL,
	[CR_WAKTU] [smalldatetime] NULL,
	[CR_TGL_STATUS] [date] NULL,
	[CR_SURAT_IJIN_NOMOR] [varchar](80) NULL,
	[CR_SURAT_IJIN_TGL_HABIS] [date] NULL,
	[CR_SURAT_IJIN_TGL_BUAT] [date] NULL,
	[U_ID] [varchar](30) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_checklist_signing]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_checklist_signing](
	[CS_ID] [int] IDENTITY(1,1) NOT NULL,
	[CL_ID] [int] NOT NULL,
	[CS_APPROVAL_TYPE] [varchar](20) NULL,
	[CS_APPROVAL_ROLE] [varchar](20) NULL,
	[CS_APPROVAL_WAKTU] [smalldatetime] NULL,
	[CS_APPROVAL_U_ID] [varchar](30) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_checklist_value_detail]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_checklist_value_detail](
	[CVD_ID] [int] IDENTITY(1,1) NOT NULL,
	[CL_ID] [int] NOT NULL,
	[RC_ID] [varchar](50) NOT NULL,
	[CVD_VALUE] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_checklist_value_item_1]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_checklist_value_item_1](
	[CVI_ID] [int] IDENTITY(1,1) NOT NULL,
	[CL_ID] [int] NOT NULL,
	[CI_ID_1] [int] NOT NULL,
	[CVI_VALUE] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_checklist_value_keterangan]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_checklist_value_keterangan](
	[CVK_ID] [int] IDENTITY(1,1) NOT NULL,
	[CL_ID] [int] NOT NULL,
	[CI_ID_1] [int] NOT NULL,
	[CVK_KETERANGAN] [varchar](800) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_mobil]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_mobil](
	[MBL_ID] [varchar](12) NOT NULL,
	[MBL_PENGELOLA] [varchar](40) NULL,
	[MBL_MERK] [varchar](30) NULL,
	[MBL_TAHUN] [varchar](4) NULL,
	[MBL_JENIS] [varchar](20) NULL,
	[MBL_KAPASITAS] [varchar](10) NULL,
	[MBL_LOKASI] [varchar](40) NULL,
	[MBL_TIPE] [varchar](20) NULL,
	[MBL_SUBTIPE] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_notifications]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_notifications](
	[N_ID] [int] IDENTITY(1,1) NOT NULL,
	[N_TGL] [datetime] NULL,
	[N_MAIL_TO] [varchar](30) NULL,
	[N_MAIL_SUBJECT] [varchar](150) NULL,
	[N_MAIL_BODY] [text] NULL,
	[N_MAIL_STATUS] [varchar](10) NULL,
	[N_MAIL_ERROR_MSG] [varchar](180) NULL,
 CONSTRAINT [PK_cl_notifications] PRIMARY KEY CLUSTERED 
(
	[N_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_references]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_references](
	[R_CATEGORY] [varchar](50) NOT NULL,
	[R_ID] [varchar](50) NOT NULL,
	[R_INFO] [varchar](200) NOT NULL,
	[R_ORDER] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_references_checklist_code]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_references_checklist_code](
	[RC_ID] [varchar](50) NOT NULL,
	[CI_ID_1] [int] NULL,
	[RC_INFO] [varchar](200) NULL,
	[RC_IS_RESULT] [varchar](20) NULL,
	[RC_IS_KETERANGAN] [varchar](20) NULL,
	[RC_MANDATORY_TYPE] [varchar](20) NOT NULL,
	[RC_ERROR_NOTE] [varchar](100) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_references_checklist_item_1]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_references_checklist_item_1](
	[CI_ID_1] [int] NOT NULL,
	[CI_TIPE_MOBIL] [varchar](20) NOT NULL,
	[CI_KRITERIA_PERIKSA] [varchar](20) NOT NULL,
	[CI_URUTAN] [int] NOT NULL,
	[CI_ITEM] [varchar](50) NOT NULL,
	[CI_ERROR_NOTE] [varchar](100) NULL,
	[CI_MANDATORY_TYPE] [varchar](20) NULL,
	[CI_IS_ATTACHMENT_MANDATORY] [varchar](30) NULL CONSTRAINT [DF_cl_references_checklist_item_1_CI_IS_ATTACHMENT_MANDATORY]  DEFAULT ('N')
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_references_checklist_item_2]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_references_checklist_item_2](
	[CI_ID_2] [int] NOT NULL,
	[CI_ID_1] [int] NOT NULL,
	[CI_URUTAN] [int] NOT NULL,
	[CI_ITEM] [varchar](200) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_settings]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_settings](
	[SET_ID] [varchar](80) NOT NULL,
	[SET_VALUE] [varchar](100) NOT NULL,
	[SET_VALUE_TEXT] [text] NULL,
	[SET_INFO] [varchar](100) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_settings_workflow]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_settings_workflow](
	[WF_TIPE_MOBIL] [varchar](20) NOT NULL,
	[WF_KRITERIA_PERIKSA] [varchar](20) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_settings_workflow_signing]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_settings_workflow_signing](
	[WF_TIPE_MOBIL] [varchar](20) NOT NULL,
	[WF_KRITERIA_PERIKSA] [varchar](20) NOT NULL,
	[WFS_APPROVAL_TYPE] [varchar](20) NOT NULL,
	[WFS_APPROVAL_ROLE] [varchar](20) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_users]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_users](
	[U_ID] [varchar](30) NOT NULL,
	[U_PASSWORD] [varchar](20) NOT NULL,
	[U_PASSWORD_HASH] [varchar](50) NULL,
	[U_NAME] [varchar](50) NOT NULL,
	[U_STATUS] [varchar](20) NOT NULL,
	[U_GROUP_ROLE] [varchar](20) NOT NULL,
	[U_LOGIN_TIME] [smalldatetime] NULL,
	[U_LOGIN_TOKEN] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cl_users_signers]    Script Date: 11/22/2017 9:43:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cl_users_signers](
	[US_ID] [int] IDENTITY(1,1) NOT NULL,
	[U_ID] [varchar](30) NOT NULL,
	[US_APPROVAL_ROLE] [varchar](20) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
